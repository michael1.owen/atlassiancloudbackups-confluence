#
# --------- Information ---------
# Script amended by Michael Owen to specific purposes for setting individual variables and uploading to AWS S3, and invoking a lambda function to send a notification to Slack.
# Based on the script created by Piotr Zadrozny
# Which is based on script created by The Epic Battlebeard 10/08/18 and downloaded from https://bitbucket.org/atlassianlabs/automatic-cloud-backup/src/master/jira_backup.py
# thanks to Lukasz Grobelny for correcting the urllib3 connection headers.
# --------- Information ---------
# 

import json
import time
import boto3
import logging
import requests
import re
import urllib3
import argparse
from botocore.exceptions import ClientError

log = logging.getLogger()
log.setLevel(logging.INFO)

print('Atlassian JIRA Python Script Started - Confluence')

time.sleep(5)

# 1 Read the input parameters.
print('Step 1 - Reading Input Parameters')

#************************************************************
# Change Below Variables
#************************************************************

# Atlassian Confluence Account Name
account = 'myatlassianaccountname'  

# Confluence username with domain something@domain.com
username = 'john.smith@mycompanyname.co.uk'

# Tells the script whether or not to pull down the attachments as well (true/false)
attachments = 'true'  

# Tells the script whether to export the backup for Cloud or Server
cloud = 'true'  

# Destination S3 bucket to store downloaded backup zipped file (Bucket name only)
bucket = 'mys3bucketname'  

# AWS region i.e. us-west-2, eu-west-1, eu-west-2
region = 'eu-west-2'  

# AWS Secrets Manager's Secret ARN which contains the Atlassian API Token (Note: We found it useful to have a seperate API Key for both JIRA and Confluence backups - this allowed them to execute concurrently).
secret_arn = 'arn:aws:secretsmanager:eu-west-2:123456789012:secret:Atlassian-Confluence-BackupAPIToken-ABCDEFGH'  

# AWS Secrets Manager's Key containing Atlassian Token
secret_key = 'Confluence-Backup'  

# AWS Lambda Function Name to push notification to Slack. We use the "lambda-to-slack" application from the AWS Serverless App Repository - https://serverlessrepo.aws.amazon.com/applications/us-east-1/289559741701/lambda-to-slack)
slack_function = 'serverlessrepo-lambda-to-slack-LambdaToSlack-ABCDEFGH' 

#************************************************************
#Change Above Variables
#************************************************************

time.sleep(5)

# 2 Create convenient variables and open new session for cookie persistence and auth
print('Step 2 - Creating Variables')
atlassian_json_data = json.dumps({"cbAttachments": attachments, "exportToCloud": cloud})  # Atlassian backup endpoint specific parameters
url = 'https://' + account + '.atlassian.net/wiki'  # Create the full base url for the JIRA instance using the account name.

time.sleep(5)

# 3 Get Atlassian user token from AWS Secret Manager. You can create token using this instruction https://confluence.atlassian.com/cloud/api-tokens-938839638.html
print('Step 3 - Getting Atlassian User Token')

session = boto3.session.Session()
client = session.client(service_name='secretsmanager', region_name=region)
try:
    response = client.get_secret_value(SecretId=secret_arn)
except ClientError as e:
    raise e
else:
    secret = response['SecretString']
    secret_json = json.loads(secret)
    token = secret_json[secret_key]

time.sleep(5)

# 4 Open new session for cookie persistence and auth
print('Step 4 - Opening Cookie / Auth Session')

session = requests.Session()
session.auth = (username, token)
session.headers.update({"Accept": "application/json", "Content-Type": "application/json"})

time.sleep(5)

try:
    
    # 5 Start generating backup
    print('Step 5 - Requesting Backup Generation')    
    backup_start = session.post(url + '/rest/obm/1.0/runbackup', data=atlassian_json_data)

    # 6a Catch error response from backup start and exit if error found.
    try:
        backup_response = int(re.search('(?<=<Response \[)(.*?)(?=\])', str(backup_start)).group(1))
    except AttributeError:
        print(backup_start.text)
        exit(1)

    # 6b Check backup startup response is 200 if not print error and exit.
    if backup_response != 200:
        print(backup_start.text)
        exit(1)
    else:
        print('Backup starting...')
    
    #Progress URL
    progress_req = session.get(url + '/rest/obm/1.0/getprogress')


    # Check for filename match in response
    file_name = str(re.search('(?<=fileName\":\")(.*?)(?=\")', progress_req.text))

    # If no file name match in JSON response keep outputting progress every 10 seconds
    while file_name == 'None':

        progress_req = session.get(url + '/rest/obm/1.0/getprogress')
        # Regex to extract elements of JSON progress response.
        file_name = str(re.search('(?<=fileName\":\")(.*?)(?=\")', progress_req.text))
        estimated_percentage = str(re.search('(?<=Estimated progress: )(.*?)(?=\")', progress_req.text))
        error = 'error'
        # While there is an estimated percentage this will be output.
        if estimated_percentage != 'None':
            # Regex for current status.
            current_status = str(
                re.search('(?<=currentStatus\":\")(.*?)(?=\")', progress_req.text).group(1))
            # Regex for percentage progress value
            estimated_percentage_value = str(
                re.search('(?<=Estimated progress: )(.*?)(?=\")', progress_req.text).group(1))
            print('Action: ' + current_status + ' / Overall progress: ' + estimated_percentage_value)
            time.sleep(10)
        # Once no estimated percentage in response the alternative progress is output.
        elif estimated_percentage == 'None':
            # Regex for current status.
            current_status = str(
                re.search('(?<=currentStatus\":\")(.*?)(?=\")', progress_req.text).group(1))
            # Regex for alternative percentage value.
            alt_percentage_value = str(
                re.search('(?<=alternativePercentage\":\")(.*?)(?=\")', progress_req.text).group(1))
            print('Action: '+ current_status + ' / Overall progress: ' + alt_percentage_value)
            time.sleep(10)
        # Catch any instance of the of word 'error' in the response and exit script.
        elif error.casefold() in progress_req.text:
            print(progress_req.text)
            exit(1)

    # Get filename from progress JSON
    
    #file_name = str(re.search('(?<=fileName\":\")(.*?)(?=\")', progress_req.text))
    file = re.search('(?<=fileName\":\")(.*?)(?=\")', progress_req.text)
    file_name = file.group(0)
    print('Filename: ' + file_name)

    # Check filename is not None
    if file_name != 'None':

        #log.info('Step 10 - Uploading Backup')
        print('Step 10 - Uploading Backup')

        downloadurl = url + '/download/' + file_name
        print('Backup file can also be downloaded from ' + downloadurl)
        
        date = time.strftime("%Y%m%d_%H%M%S")
        print("Date: " + date)

        key = 'Atlassian/Confluence/' + account + '_confluence_backup_' + date + '.zip'
        print('Key: ' + key)
    
        s3=boto3.client('s3')
        http=urllib3.PoolManager()
        headers = urllib3.make_headers(basic_auth=username + ":" + token)

        print('***UploadS3FileMarker***')
        print('*Download URL: ' + downloadurl)
        print('*Bucket: ' + bucket)
        print('*Key: ' + key)

        s3.upload_fileobj(http.request('GET', downloadurl, preload_content=False, headers=headers), bucket, key)
    
        print('Complete - Sending Success Message')

        lambda_client = boto3.client('lambda')
        lambda_payload = json.dumps(["The Confluence backup to AWS S3 has been successfully completed."])
        lambda_client.invoke(FunctionName=slack_function, 
                     InvocationType='Event',
                     Payload=lambda_payload)
        #ProcessEnd
        print('***Process Ended - Backup Complete***')
        
except Exception as e:
    # 11 Creating notification in case of failure
    print('Exception Raised')

    lambda_client = boto3.client('lambda')
    lambda_payload = json.dumps(["The Confluence backup to AWS S3 has failed - please see the CloudWatch Logs for error tracing."])
    lambda_client.invoke(FunctionName=slack_function, 
                     InvocationType='Event',
                     Payload=lambda_payload)
    raise e
    
